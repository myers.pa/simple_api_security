require 'rails/generators/migration'

module SimpleAuth
  module Generators
    class InstallGenerator < ::Rails::Generators::Base
      argument :client_model, type: :string
      argument :token_expiration, type: :string, default: 300
      argument :replay_attack_expiration, type: :string, default: 300

      desc "Install SimpleAuth"
      include Rails::Generators::Migration
      source_root File.expand_path('../templates', __FILE__)

      def self.next_migration_number(path)
        unless @prev_migration_nr
          @prev_migration_nr = Time.now.utc.strftime("%Y%m%d%H%M%S").to_i
        else
          @prev_migration_nr += 1
        end
        @prev_migration_nr.to_s
      end

      def copy_migrations
        migration_template "create_client_token_table.rb", "db/migrate/create_client_token_table.rb"
      end

      def setup_initializer
        template "initializer.rb", "config/initializers/simple_auth.rb"
      end

    end
  end
end
