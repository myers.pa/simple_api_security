################################################################################
#                          SimpleAuth Configurations                           #
################################################################################
# Ex.                                                                          #
# SimpleAuth.configure do |config|                                             #
#   config.replay_attack_expiration = 300                                      #
#   config.client_model = MyUserModel                                          #
# end                                                                          #
#                                                                              #
# Configurables.                                                               #
#   - replay_attack_expiration: The maximum amount of time allowed between a   #
#       client signature and the server receiving the request before the       #
#       request is flagged as a replay attack. (in seconds)                    #
#   - client_model: The user client model.
################################################################################
SimpleAuth.configure do |config|
  config.client_model = <%= client_model %>
  config.replay_attack_expiration = <%= replay_attack_expiration %>
end
