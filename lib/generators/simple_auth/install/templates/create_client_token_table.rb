class CreateClientTokenTable < ActiveRecord::Migration
  def self.up
    create_table :client_tokens do |t|
      t.belongs_to :<%= client_model.underscore.to_sym %>
      t.string  :public_token, unique: true
      t.string  :private_token, unique: true
      t.timestamps
    end
  end

  def self.down
    drop_table :client_tokens
  end
end
