module SimpleAuth
  module SignatureValidatable
    extend ActiveSupport::Concern

    included do
    end # End included

    class_methods do

      def validate_token!
        token = request.headers['X-Api-Key']
        signature = request.headers['X-Authorization']
        timestamp = request.headers['X-Request-Time']
        method = request.method.to_s.upcase

        # Verify necessary headers are present
        unless(token && signature && timestamp)
          unauthorized! and return false
        end

        # Verify the public key provided is tied to a client
        client = SimpleAuth.configuration.client_model.by_public_token(token)
        unless(client.present?)
          unauthorized! and return false
        end

        # Check that the signature is valid
        private_key = client.client_token.private_token
        signature_replica = construct_signature(method, token, private_key, timestamp, params)
        unless(signature == signature_replica)
          unauthorized! and return false
        end

        # Check the timestamp for replay attack mitigation
        if(SimpleAuth.configuration.replay_attack_expiration > -1)
          now = Time.now.utc.to_i
          delta = now - timestamp
          unless(delta >= 0 && delta < SimpleAuth.configuration.replay_attack_expiration)
            unauthorized! and return false
          end
        end
      end

      def unauthorized!
        respond_to do |format|
          format.json { render json: { error: "Unauthorized" }.to_json, status: :unauthorized }
          format.xml { render xml: { error: "Unauthorized" }.to_xml, status: :unauthorized }
        end
      end

      def construct_signature(method, public_token, private_token, timestamp, args)
        sig_string = "#{method} #{public_token} #{timestamp} " +
                       args
                        .sort_by { |key, value| [key.to_s, value.to_s] }
                        .collect { |key, value| "#{key}:#{value}" }
                        .join(',')
        Base64.encode64(OpenSSL::HMAC.digest(OpenSSL::Digest.new('sha256'), private_token, sig_string)).strip()
      end

    end # End class_methods

  end
end

ActionController::Base.send :include, SimpleAuth::SignatureValidatable
