module SimpleAuth

  class Engine < ::Rails::Engine

    require 'simple_auth/token_authenticatable'
    require 'simple_auth/signature_validatable'

    config.after_initialize do
      require 'simple_auth/client_token'
    end

  end

end
