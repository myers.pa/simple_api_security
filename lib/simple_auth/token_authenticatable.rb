module SimpleAuth
  module TokenAuthenticatable
    extend ActiveSupport::Concern

    included do
    end # End included

    class_methods do

      def is_token_authenticatable
        class_eval do
          has_one :client_token, :class_name => "SimpleAuth::ClientToken"
          before_create { self.client_token = SimpleAuth::ClientToken.new }
        end
      end

      def by_public_token(public_token)
        t = SimpleAuth::ClientToken.find_by_public_token(public_token)
        if(t.present?)
          t.send(SimpleAuth.configuration.client_model.name.underscore.to_sym)
        else
          nil
        end
      end

    end # End ClassMethods
  end
end

ActiveRecord::Base.send :include, SimpleAuth::TokenAuthenticatable
