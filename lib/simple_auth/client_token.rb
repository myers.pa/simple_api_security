module SimpleAuth
  class ClientToken < ::ActiveRecord::Base
    belongs_to SimpleAuth.configuration.client_model.name.underscore.to_sym
    before_create { self.private_token = generate_secure_token; self.public_token = generate_secure_token }

    def regenerate_private_key
      update private_key: generate_secure_token
    end

    def regenerate_public_key
      update public_key: generate_secure_token
    end

    private
    def generate_secure_token
      SecureRandom.base64(24)
    end

  end
end
