require 'gem_config'

module SimpleAuth
  include GemConfig::Base

  with_configuration do
    has :client_model
    has :replay_attack_expiration, classes: Integer
  end

  require 'simple_auth/engine'

end
