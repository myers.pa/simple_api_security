$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "simple_auth/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "simple_auth"
  s.version     = SimpleAuth::VERSION
  s.authors     = ["Peter Myers"]
  s.email       = ["myers.pa@gmail.com"]
  s.homepage    = "https://www.gitlab.com"
  s.summary     = "Summary of SimpleAuth."
  s.description = "Description of SimpleAuth."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.3"
  s.add_dependency 'gem_config'

  s.add_development_dependency "sqlite3"
end
